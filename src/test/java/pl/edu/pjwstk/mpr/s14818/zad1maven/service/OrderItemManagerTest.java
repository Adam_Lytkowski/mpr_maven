package pl.edu.pjwstk.mpr.s14818.zad1maven.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.s14818.zad1maven.domain.OrderItem;

public class OrderItemManagerTest {

OrderItemManager orderItemManager = new OrderItemManager();
	
	private final static String NAME = "Mint tea";
	private final static String DESCRIPTION = "The best mint tea you can get!";
	private final static double PRICE = 25.50;
	
	@Test
	public void checkConnection() {
		assertNotNull(orderItemManager.getConnection());
	}
	
	@Test
	public void checkAdding() {
		OrderItem orderItem = new OrderItem(NAME, DESCRIPTION, PRICE);
		
		orderItemManager.clearOrderItem();
		assertEquals(1, orderItemManager.addOrderItem(orderItem));
		
		List<OrderItem> orderItems = orderItemManager.getAllOrderItems();
		OrderItem orderItemRetrieved = orderItems.get(0);
		
		assertEquals(NAME, orderItemRetrieved.getName());
		assertEquals(DESCRIPTION, orderItemRetrieved.getDescription());
		assertEquals(PRICE, orderItemRetrieved.getPrice(), 0.01);
	}

}
