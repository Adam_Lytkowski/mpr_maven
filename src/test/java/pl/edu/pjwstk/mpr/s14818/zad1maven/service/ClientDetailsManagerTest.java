package pl.edu.pjwstk.mpr.s14818.zad1maven.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.s14818.zad1maven.domain.ClientDetails;

public class ClientDetailsManagerTest {

ClientDetailsManager clientDetailsManager = new ClientDetailsManager();
	
	private final static String NAME = "Salvador";
	private final static String SURNAME = "Avila";
	private final static String LOGIN = "sal96";
	
	@Test
	public void checkConnection() {
		assertNotNull(clientDetailsManager.getConnection());
	}
	
	@Test
	public void checkAdding() {
		ClientDetails clientDetails = new ClientDetails(NAME, SURNAME, LOGIN);
		
		clientDetailsManager.clearClientDetails();
		assertEquals(1, clientDetailsManager.addClientDetails(clientDetails));
		
		List<ClientDetails> clientDetailsList = clientDetailsManager.getAllClientDetails();
		ClientDetails clientDetailsRetrieved = clientDetailsList.get(0);
		
		assertEquals(NAME, clientDetailsRetrieved.getName());
		assertEquals(SURNAME, clientDetailsRetrieved.getSurname());
		assertEquals(LOGIN, clientDetailsRetrieved.getLogin());
	}

}
