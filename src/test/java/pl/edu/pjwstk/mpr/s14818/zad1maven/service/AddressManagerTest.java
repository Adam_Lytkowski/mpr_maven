package pl.edu.pjwstk.mpr.s14818.zad1maven.service;

import static org.junit.Assert.*;
import org.junit.Test;
import pl.edu.pjwstk.mpr.s14818.zad1maven.domain.Address;
import java.util.List;

public class AddressManagerTest {

	AddressManager addressManager = new AddressManager();
	
	private final static String STREET = "Meridiana";
	private final static String BUILDING_NUMBER = "72";
	private final static String FLAT_NUMBER = "1010";
	private final static String POSTAL_CODE = "08018";
	private final static String CITY = "Barcelona";
	private final static String COUNTRY = "Catalunya";
	
	@Test
	public void checkConnection() {
		assertNotNull(addressManager.getConnection());
	}
		
	@Test
	public void checkAdding() {
		Address address = new Address(STREET, BUILDING_NUMBER, FLAT_NUMBER, POSTAL_CODE, CITY, COUNTRY);
		
		addressManager.clearAddress();
		assertEquals(1, addressManager.addAddress(address));
		
		List<Address> addresses = addressManager.getAllAddresses();
		Address addressRetrieved = addresses.get(0);
		
		assertEquals(STREET, addressRetrieved.getStreet());
		assertEquals(BUILDING_NUMBER, addressRetrieved.getBuildingNumber());
		assertEquals(FLAT_NUMBER, addressRetrieved.getFlatNumber());
		assertEquals(POSTAL_CODE, addressRetrieved.getPostalCode());
		assertEquals(CITY, addressRetrieved.getCity());
		assertEquals(COUNTRY, addressRetrieved.getCountry());
	}
	

}
