package pl.edu.pjwstk.mpr.s14818.zad1maven.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import pl.edu.pjwstk.mpr.s14818.zad1maven.domain.Address;
import pl.edu.pjwstk.mpr.s14818.zad1maven.domain.ClientDetails;
import pl.edu.pjwstk.mpr.s14818.zad1maven.domain.Order;

public class OrderManagerTest {

	OrderManager orderManager = new OrderManager();
	
	ClientDetails client1 = new ClientDetails(1);
	Address address1 = new Address(1);
	
	@Test
	public void checkConnection() {
		assertNotNull(orderManager.getConnection());
	}
		
	@Test
	public void checkAdding() {
		Order order = new Order(client1, address1);
		
		orderManager.clearOrder();
		assertEquals(1, orderManager.addOrder(order));
		
		List<Order> orders = orderManager.getAllOrders();
		Order orderRetrieved = orders.get(0);
		
		assertEquals(client1.getId(), orderRetrieved.getClient().getId());
		assertEquals(address1.getId(), orderRetrieved.getDeliveryAddress().getId());

	}

}
