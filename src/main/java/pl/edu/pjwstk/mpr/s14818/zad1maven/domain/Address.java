package pl.edu.pjwstk.mpr.s14818.zad1maven.domain;

public class Address {
	private long id;
	private String street;
	private String buildingNumber;
	private String flatNumber;
	private String postalCode;
	private String city;
	private String country;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getBuildingNumber() {
		return buildingNumber;
	}
	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}
	public String getFlatNumber() {
		return flatNumber;
	}
	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Address() {}
	public Address(long id) {
		this.id = id;
	}
	
	public Address(String street, String buildingNumber,
			String flatNumber, String postalCode, String city, String country) {
		this.street = street;
		this.buildingNumber = buildingNumber;
		this.flatNumber = flatNumber;
		this.postalCode = postalCode;
		this.city = city;
		this.country = country;
	}

	
}
