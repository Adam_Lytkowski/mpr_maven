package pl.edu.pjwstk.mpr.s14818.zad1maven.domain;
import java.util.List;

public class Order {
	
private long id;
private ClientDetails client;
private Address deliveryAddress;
private List<OrderItem> items;


public long getId() {
	return id;
}
public void setId(long id) {
	this.id = id;
}
public ClientDetails getClient() {
	return client;
}
public void setClient(ClientDetails client) {
	this.client = client;
}
public Address getDeliveryAddress() {
	return deliveryAddress;
}
public void setDeliveryAddress(Address deliveryAddress) {
	this.deliveryAddress = deliveryAddress;
}
public List<OrderItem> getItems() {
	return items;
}
public void setItems(List<OrderItem> items) {
	this.items = items;
}

public Order() {}

public Order(ClientDetails client, Address deliveryAddress) {
	this.client = client;
	this.deliveryAddress = deliveryAddress;
}

public Order(ClientDetails client, Address deliveryAddress, List<OrderItem> items) {
	this.client = client;
	this.deliveryAddress = deliveryAddress;
	this.items = items;
}
}
